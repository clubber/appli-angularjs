angular.module('Movment')

  .controller('StartCtrl', function ($rootScope,$ionicSlideBoxDelegate, $scope, $state, AuthService, $ionicModal, $ionicPlatform, MODE) {
    $ionicPlatform.ready(function () {
      if (!MODE.debug){
        intercom.registerUnidentifiedUser();
      }
    });

    function getStarted(){
    var value = window.localStorage.getItem('started');
   if (value == null || value == 0){
    $ionicModal.fromTemplateUrl('templates/startup.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(startup) {
      $scope.phone = ionic.Platform.isIOS();
      $scope.startup = startup;
      $scope.startup.show();
    });
    }
    }

    $scope.connectWithGG = function () {
      intercom.logEvent('Login with Google');
    };

    getStarted();

    $scope.slideChanged = function (index) {
      $scope.slideIndex = index;
      if (index == 2){
        window.localStorage.setItem('started', 1);
        $scope.startup.hide();
      }
    };

    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };

    $scope.close = function () {
      window.localStorage.setItem('started', 1);
      $scope.startup.hide();
    };

    $ionicModal.fromTemplateUrl('templates/modalCfg.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.cfgModal = function () {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.connectWithFB = function () {
      intercom.logEvent("login with facebook");
      facebookConnectPlugin.login(['public_profile', 'email', 'user_friends'], function (fb) {
        if(fb.status == 'connected'){
          AuthService.FBlogin(fb.authResponse.accessToken, function (data) {
            $rootScope.$emit('login');
            var user = data.result;
            var value = window.localStorage.getItem('tagg');
            if (value == null || value == 0 && (user.local.musicChoices.length == 0 && user.local.eventChoices.length == 0)){
              $state.go('preprofile');
            } else {
              $state.go('app.centrale');
            }
            /*intercom.registerIdentifiedUser({ userId: user._id, email: user.facebook.email });
            intercom.updateUser({ email: user.facebook.email, name: user.facebook.firstName + ' ' + user.facebook.lastName });*/
          });
        }
      }, function (err) {
        console.log(err);
      });
    };
  })

  .controller('EventCtrl', function ($scope,$sce, $ionicPopup, $ionicHistory, MODE, $ionicScrollDelegate, $cordovaGoogleAnalytics, $rootScope, $ionicLoading, $cordovaLocalNotification, $state, $stateParams, EventService, UserService, $window, $ionicPopover) {
    var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
    $scope.filterhidden = true;

    $scope.playVideo = function () {
      if (ionic.Platform.isIOS()){
        var video = $('video')[0];
        video.play();
      } else {
        VideoPlayer.play($scope.video);
      }

    };

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.filterhidden = true;
        })
      }
    };

    var reminder = 40;
    $scope.bouncing = true;
    $scope.header = true;
    var toswitch = false;

    $ionicPopover.fromTemplateUrl('templates/phone.html', {
      scope: $scope
    }).then(function (popover) {
      $scope.popover = popover;
    });

    $scope.openPopover = function ($event) {
      $scope.popover.show($event);
    };
    $scope.closePopover = function () {
      $scope.popover.hide();
    };

      $scope.gstatus = 0;
      $scope.istatus = 0;

    $scope.exit = function () {
      $ionicHistory.goBack();
    };

     function getStatus(){
       $ionicLoading.show({
         template: 'Loading...'
       });
       $scope.gstatus = 0;
       $scope.istatus = 0;
      UserService.getUserActivities(function (result) {
       var activities = result.data.activities.events;
        for (var i = 0; i < activities.length; i++){
          if (activities[i].event == $stateParams.id){
            if (activities[i].value == "going"){
              $scope.gstatus = 1;
            } else {
              $scope.istatus = 1;
            }
          }
        }
        $ionicLoading.hide();
      });
    }

    getStatus();

    EventService.getEventById($stateParams.id, function (result) {
      if (!MODE.debug) {
        window.mixpanel.track('tracking event', { event : result.events.name, eventId: result.events._id  });
        intercom.logEvent("Consult event", {
          dateAtAction: moment(),
          eventName: result.events.name,
          eventId: result.events._id
        });
        $cordovaGoogleAnalytics.trackEvent("Consult event", result.events.name, result.events._id);
      }
      $scope.event = result.events;
      $scope.video = $sce.trustAsResourceUrl('http://62.210.108.100:8000/video/' + $scope.event.teaser);
    });

    $scope.call = function (num) {
      if (!MODE.debug) {

        window.mixpanel.track('call event', { event : $scope.event.name, eventId: $scope.event._id });
        intercom.logEvent("Consult event call", {
          dateAtAction: moment(),
          eventName: $scope.event.name,
          eventId: $scope.event._id
        });
        $cordovaGoogleAnalytics.trackEvent("Consult call", $scope.event.name, $scope.event._id);
      }
      console.log(num);
      window.open('tel:' + num.replace(/ /g,''), '_system');
    };


    $scope.winTicket = function () {




    };


    $scope.map = function () {
      if (!MODE.debug) {

        window.mixpanel.track('map event', { event : $scope.event.name, eventId: $scope.event._id  });

        intercom.logEvent("Consult map call", {
          dateAtAction: moment(),
          eventName: $scope.event.name,
          eventId: $scope.event._id
        });

        $cordovaGoogleAnalytics.trackEvent("Consult map", $scope.event.name, $scope.event._id);
      }
      $state.go('app.map', {position: $scope.event.place[0].latitude + '|' + $scope.event.place[0].longitude});
    };

    $scope.link = function (tel) {
      $window.open('tel://' + tel.trim());
    };


    $scope.interest = function (value) {

       $ionicPopup.alert({
        title: 'A little party never killed nobody',
        template: 'Hey buddy! Because we love you, I will remind you 10 hours before the party. So no stress 😉  - Jack'
      });

      if (!MODE.debug) {
        window.mixpanel.track('interest event', { event : $scope.event.name, eventId: $scope.event._id  });
        intercom.logEvent("Interest event", {
          dateAtAction: moment(),
          eventName: $scope.event.name,
          eventId: $scope.event._id
        });
        $cordovaGoogleAnalytics.trackEvent("Event interest", $scope.event.name, $scope.event._id);
      }
      var data = {
        value: value,
        eventId: $stateParams.id
      };

      var notif = {
        id: $scope.event.notificationId,
        title: 'It\'s time to party!',
        firstAt: moment($scope.event.dateStart).subtract(8, 'h').toDate(),
        text: 'Votre soirée ' + $scope.event.name + ' est dans quelques heures. 🚀',
        icon : "res://img/Logo.png",
        data: {
          eventId: data.eventId
        }
      };
      if (!MODE.debug){
        $cordovaLocalNotification.schedule(notif).then(function (result) {
          wishlist.push($scope.event);
          window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
          $rootScope.$emit('Wishlist');
          EventService.setGoing(data, function (result) {
            getStatus();
          });
        });
      } else {
        wishlist.push($scope.event);
        window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
        $rootScope.$emit('Wishlist');
        EventService.setGoing(data, function (result) {
          getStatus();
        });
      }

    };

    $scope.desinterest = function () {
      if (!MODE.debug) {
        window.mixpanel.track('desinterest event', { event : $scope.event.name, eventId: $scope.event._id  });
        $cordovaGoogleAnalytics.trackEvent("Event desinterest", $scope.event.name, $scope.event._id);
        intercom.logEvent("Desinterest event", {
          dateAtAction: moment(),
          eventName: $scope.event.name,
          eventId: $scope.event._id
        });
      }
      var data = {
        eventId: $stateParams.id
      };

      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == $scope.event._id) {
          wishlist.splice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
      $rootScope.$emit('Wishlist');
      if (!MODE.debug){
        $cordovaLocalNotification.cancel($scope.event.notificationId).then(function (result) {
          EventService.rmGoing(data, function (result) {
            getStatus();
          });
        });
      } else {
        EventService.rmGoing(data, function (result) {
          getStatus();
        });
      }

      $rootScope.$emit('Wishlist');
    }
  })

  .controller('friendsCtrl', function ($scope, UserService, $cordovaGoogleAnalytics, $cordovaSocialSharing, MODE) {

    $scope.inviteThisFriends = function () {

    };

    $scope.inviteNewFriends = function () {
      if (!MODE.debug) {
        $cordovaGoogleAnalytics.trackEvent("Share to a friend");
        intercom.logEvent("Share to a friend");
      }
      $cordovaSocialSharing
        .share("Voici une appli qui devrait te plaire ;-), télécharge movment et trouve la soirée qu'il te faut", "Message", null, "http://goo.gl/F8CGmb")
        .then(function(result) {
          // Success!
        }, function(err) {
          console.log(err);
          // An error occurred. Show a message to the user
        });
    }


  })


  .controller('HousePartyCtrl', function ($scope, $state, $filter, EventService, $ionicScrollDelegate,$timeout, TAG) {
    $scope.filters = [];
    EventService.getEventType("House party").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      $timeout(function () {
        $scope.nodata = true;
      });

      var events = result.data.events;
      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);

    });

    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    $scope.musictype = TAG.genre;
    $scope.pricing = TAG.price;
    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          var newWishlist = wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("HouseParty").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('ConcertCtrl', function ($scope, $state, EventService,$filter, $ionicScrollDelegate,$timeout, TAG) {
    $scope.filters = [];
    $scope.nodata = false;
    EventService.getEventType("Concert").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      $timeout(function () {
        $scope.nodata = true;
      });
      var events = result.data.events;
      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);

    });

    $scope.data = {};

    $scope.data.filterhidden = false;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    $scope.filters = [];

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          var newWishlist = wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("Concert").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('nightsCtrl', function ($scope, $state, EventService, $ionicHistory, $ionicScrollDelegate,$timeout, TAG, $filter) {

    $scope.data = {};
    $scope.nodata = false;
    $scope.data.filterhidden = true;
    $scope.filters = [];

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getTonightEvent(function (result) {
      $scope.events = $filter('timeSort')(result.events);
      var events = result.events;
      $timeout(function () {
        $scope.nodata = true;
      });

      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);

    });

    $scope.filters = [];

    $scope.musictype = TAG.genre;
    $scope.pricing = TAG.price;
    $scope.partytype = TAG.type;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
         var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };


   $scope.doRefresh = function () {
     EventService.getTonightEvent(function (result) {
       $scope.events = result.events;
     });
       $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('pubcrawlCtrl', function ($scope, $state, EventService, $filter, $ionicHistory, $ionicScrollDelegate,$timeout, TAG) {

    $scope.filters = [];

    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getEventType("Barathon").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      $scope.events = result.data.events;
      var events = result.data.events;
      $timeout(function () {
        $scope.nodata = true;
      });

      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);
    });

    $scope.musictype = TAG.genre;
    $scope.pricing = TAG.price;
    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("Pubcrawl").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('weekendCtrl', function ($scope, $state,$timeout, EventService, $filter, $ionicHistory, $ionicScrollDelegate, TAG) {
    $scope.filters = [];

    EventService.getWeekendEvent().then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      var events = result.data.events;
      $timeout(function () {
        $scope.nodata = true;
      });
      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);

    });
    $scope.nodata = false;
    $scope.filters = [];

    $scope.weekend = TAG.weekend;
    $scope.musictype = TAG.genre;
    $scope.pricing = TAG.price;
    $scope.partytype = TAG.type;

    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getWeekendEvent().then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('afterworkCtrl', function ($scope, $state, EventService, $ionicHistory, $filter, $ionicScrollDelegate,$timeout, TAG) {
    $scope.nodata = false;

    $scope.filters = [];
    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getEventType("Afterwork").then(function (result) {
      $scope.events = result.data.events;
      //var events = result.data.events;
      $timeout(function () {
        $scope.nodata = true;
      });
      $scope.$watch('filters', function (n, o) {
        var events = result.data.events;
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);

    });

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('karaokeCtrl', function ($scope, $state, EventService, $ionicHistory, $ionicScrollDelegate, $filter, $timeout, TAG) {
    $scope.nodata = false;
    $scope.filters = [];
    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getEventType("Student party").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      var events = result.data.events;
      $timeout(function () {
        $scope.nodata = true;
      });

      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);
    });

    $scope.filters = [];

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("Karaoke").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('clubbingCtrl', function ($scope, $state, EventService, $ionicHistory, $ionicScrollDelegate, $filter, $timeout, TAG) {
    $scope.nodata = false;
    $scope.filters = [];
    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getEventType("Clubbing").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      var events = $scope.events = $filter('timeSort')(result.data.events);
      $timeout(function () {
        $scope.nodata = true;
      });

      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);
    });

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++){
        if (wishlist[i]._id == event._id){
          var newWishlist =  wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("Clubbing").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('tomorrowCtrl', function ($scope, $state, EventService,$filter, $ionicScrollDelegate,$timeout, TAG) {

    $scope.filters = [];
    $scope.nodata = false;
    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getTomorrowEvent(function (result) {
      $scope.events = $filter('timeSort')(result.events);
      $timeout(function () {
        $scope.nodata = true;
      });
      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(result.events);
        }
      }, true);
    });

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.type;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          var newWishlist = wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getTomorrowEvent(function (result) {
        $scope.events = result.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('AfterCtrl', function ($scope, $state, EventService,$filter, $ionicScrollDelegate, $timeout, TAG) {

    $scope.nodata = false;

    $scope.data = {};

    $scope.data.filterhidden = true;

    $scope.getScrollPosition = function () {
      if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top > 50) {
        $scope.$apply(function () {
          $scope.data.filterhidden = false;
        })
      } else if ($ionicScrollDelegate.$getByHandle('homeScroll').getScrollPosition().top < -20) {
        $scope.$apply(function () {
          $scope.data.filterhidden = true;
        })
      }
    };

    EventService.getEventType("After").then(function (result) {
      $scope.events = $filter('timeSort')(result.data.events);
      $timeout(function () {
        $scope.nodata = true;
      });
      var events = result.data.events;
      $scope.$watch('filters', function (n, o) {
        if (n.length){
          $scope.events = $filter('Type')(events, n);
        }
        else {
          $scope.events = $filter('timeSort')(events);
        }
      }, true);
    });

    $scope.filters = [];

    $scope.musictype = TAG.genre;

    $scope.pricing = TAG.price;

    $scope.partytype = TAG.date;

    $scope.getWishlist = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          return 1;
        }
      }
      return 0;
    };

    $scope.addFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      wishlist.push(event);
      window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
    };

    $scope.rmFavoris = function (event) {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      for (var i = 0; i < wishlist.length; i++) {
        if (wishlist[i]._id == event._id) {
          var newWishlist = wishlist.slice(i, 1);
        }
      }
      window.localStorage.setItem('Wishlist', JSON.stringify(newWishlist));
    };

    $scope.doRefresh = function () {
      EventService.getEventType("After").then(function (result) {
        $scope.events = result.data.events;
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('FavCtrl', function ($scope, $rootScope, $state) {
    $scope.filters = [];
    var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
    $scope.events = wishlist;


    $rootScope.$on('Wishlist', function () {
      var wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      $scope.events = wishlist;
    });


    $scope.doRefresh = function () {
      wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];
      $scope.events = wishlist;
      $scope.$broadcast('scroll.refreshComplete');
    };

  })

  .controller('SignupCtrl', function ($scope, $state, AuthService, $window, $ionicLoading) {

    $scope.back = function () {
      $window.history.back();
    };

    $scope.data = {
      signup: signup,
      email: '',
      password: ''
    };

    function signup() {
      $scope.data.email = $scope.data.user.email.trim();
      $scope.data.password = $scope.data.user.password;

      if ($scope.data.user.email != '' && $scope.data.user.password != '') {
        $ionicLoading.show({
          template: 'Loading...'
        });
        AuthService.register($scope.data).then(function (result) {
          $ionicLoading.hide();
          if (result.data.success) {
            AuthService.login($scope.data.user, function (result) {
              window.localStorage.setItem('userProfile', JSON.stringify(result.data.user));
              var user = result.data.user;
              intercom.registerIdentifiedUser({userId: user._id, email: user.local.email});
              $state.go('preprofile');
            });
          } else {
            $scope.error = result.data.msg;
          }
        })
      }
    }

  })

  .controller('ConnexionCtrl', function ($scope, $rootScope, $state, AuthService, $window, $ionicLoading, MODE, $cordovaInAppBrowser, $cordovaFacebook) {

    $scope.back = function () {
      $state.go('Start');
    };

    $scope.data = {
      'email': null,
      'password': null
    };

    $scope.connect = function () {
      if (!MODE.debug){
        intercom.logEvent("login Email/Password");
      }

      $ionicLoading.show({
        template: 'Loading...'
      });
      AuthService.login($scope.data, function (result) {
        $rootScope.$emit('login');
        if (result.data.success == false) {
          $ionicLoading.hide();
          $scope.error = result.data.message;
        } else {
          $ionicLoading.hide();
          $state.go('app.centrale');
        }
      })
    }
  })

  .controller('AppCtrl', function ($ionicUser, $ionicAuth, $rootScope, MODE, $ionicTabsDelegate, $ionicSideMenuDelegate, AuthService, $state, $cordovaCamera, $scope,Utils, API_ENDPOINT, $ionicPlatform, PluginService, UserService, $cordovaImagePicker, $cordovaFileTransfer) {

    var tab =[];
    tab[0] = 'tabs-yellow';
    tab[1] = 'tabs-purple';
    tab[2] = 'tabs-red';
    tab[3] = 'tabs-green';

    $rootScope.$on('login', function(){
      var user = JSON.parse(window.localStorage.getItem('userProfile'));
      if (user && angular.isDefined(user) && !MODE.debug){
        var details = {'email': user.local.email ? user.local.email : user.facebook.email,
          'password': user._id };
        if (!$ionicAuth.isAuthenticated()) {
          $ionicAuth.login('basic', details).then(function () {
            console.log('Auth success');
          }, function (err) {
            console.log('Auth Failed');
            $ionicAuth.signup(details).then(function() {
              console.log('register success');
            }, function(err) {
              console.log('register Failed');
            });
          });
        }
        $ionicUser.set('userId', user._id);
        $ionicUser.set('Name', angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName + ' ' + user.local.lastName : user.facebook.firstName + ' ' + user.facebook.lastName);
        $ionicUser.save();
        intercom.registerIdentifiedUser({ userId: user._id, email: user.local.email ? user.local.email : user.facebook.email });
        intercom.updateUser({ email: user.local.email ? user.local.email : user.facebook.email, name: angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName + ' ' + user.local.lastName : user.facebook.firstName + ' ' + user.facebook.lastName });
      }
    });

    $scope.getTab = function () {
     return tab[$ionicTabsDelegate.selectedIndex()];
    };

    $scope.gotoProfile = function() {
      $ionicSideMenuDelegate.toggleLeft();
      $state.go('app.profile');
    };

    $ionicPlatform.ready(function() {
      PluginService.getPosition(function (coords) {
        var position = {
          latitude: coords.latitude,
          longitude: coords.longitude
        };
        window.localStorage.setItem('location', JSON.stringify(position));
      });
    });


    $scope.uploadPicture = function () {
      var ProfilePicture = Utils.createUuid() + '.jpg';

      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 200,
        targetHeight: 200
      };

      $cordovaCamera.getPicture(options).then(function(imageUri) {
        $cordovaFileTransfer.upload(API_ENDPOINT.url + '/upload/avatar/', imageUri, {fileName : ProfilePicture}).then(function(result) {
          var user = JSON.parse(window.localStorage.getItem('userProfile'));
          user.local.pict.unshift(ProfilePicture);
          UserService.updateProfile(user, function (res) {
            var NewUser = res.data.msg;
            window.localStorage.setItem('userProfile', JSON.stringify(NewUser));
            $rootScope.$emit('image:update');
          });
        });
      }, function(err) {
        // error
      });
    };
  })

  .controller('CentraleCtrl', function ($ionicUser, $rootScope, AuthService, $cordovaVibration, $state, $scope, MODE, $ionicModal, EventService, $cordovaGoogleAnalytics, $ionicPlatform, $cordovaDialogs, $timeout, notificationService, $cordovaLocalNotification) {


    if (!MODE.debug){
            $ionicPlatform.ready(function () {
       if (ionic.Platform.isIOS()){
          intercom.registerForPush()
        } else {
          intercom.registerForPush('608995360280');
        }

        $cordovaLocalNotification.getAllTriggered().then(function(notifs){
          $scope.notifs = notifs;
        });
      });
    }

    $ionicModal.fromTemplateUrl('templates/notification.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openNotif = function () {
      $scope.modal.show();
    };

    $scope.closeNotif = function() {
      $scope.modal.hide();
    };

    $scope.goToEventLocalNotif = function(encodedId){
      var event = JSON.parse(encodedId);
      $scope.modal.hide();
      $state.go('app.event', { id : event.eventId });
    };

    $scope.goToEvent = function(notif){
      $scope.modal.hide();
      notificationService.readNotif(notif._id, function () {
        switch (notif.type) {
          case 'Event':
            $state.go('app.event', {id: notif.payload.id});
            break;
          case 'friends':
            break;
          case 'snapComment':
            $state.go('snapComment', {id: notif.payload.id});
            break;
        }
        $state.go('app.event', { id : notif.payload.eventId });
      });
    };

    $ionicPlatform.ready(function () {
      var user = JSON.parse(window.localStorage.getItem('userProfile'));
      if (user && user.facebook){
        AuthService.FBupdate(function (result) {
          if (!result.success){
            if (!MODE.debug) {
              intercom.reset();
              intercom.registerUnidentifiedUser();
              $cordovaGoogleAnalytics.trackEvent("logout");
            }
            AuthService.logout();
            $rootScope.$emit('image:update');
            $state.go('Start');
          }
        });
      }
      if (angular.isDefined(user) && !MODE.debug) {
        var details = {'email': user.local.email ? user.local.email : user.facebook.email,
          'password': user._id };
        if (!$ionicAuth.isAuthenticated()) {
          $ionicAuth.login('basic', details).then(function () {
          }, function (err) {
            $ionicAuth.signup(details).then(function() {
            }, function(err) {
            });
          });
        }
        $ionicUser.set('userId', user._id);
        $ionicUser.set('Name', angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName + ' ' + user.local.lastName : user.facebook.firstName + ' ' + user.facebook.lastName);
        $ionicUser.save();
       window.mixpanel.people.set({
          "$email": user.local.email ? user.local.email : user.facebook.email,    // only special properties need the $
          "$created": user.local.registrationDate,
          "$firstname": angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName : user.facebook.firstName,
          "$lastname": angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.lastName : user.facebook.lastName,
          "$last_login": new Date()
        });
        intercom.updateUser({ email: user.local.email ? user.local.email : user.facebook.email, name: angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName + ' ' + user.local.lastName : user.facebook.firstName + ' ' + user.facebook.lastName });
        $cordovaGoogleAnalytics.trackEvent("Login", "click", angular.isDefined(user.local) && angular.isDefined(user.local.firstName) ? user.local.firstName + ' ' + user.local.lastName : user.facebook.firstName + ' ' + user.facebook.lastName);
      }
      $timeout(function () {
        notificationService.pushSub(ionic.Platform.isIOS() ? 'ios' : 'android', user._id);
      }, 2000);
      $rootScope.$emit('image:update');
    });

  })

  .controller('ProfileCtrl', function ($rootScope,notificationService,$cordovaLocalNotification, $scope,$ionicPlatform, $timeout, $ionicSideMenuDelegate, $ionicHistory, $cordovaImagePicker, $cordovaCamera, PluginService, TAG, UserService, $filter, $cordovaFileTransfer, API_ENDPOINT, Utils, $state, $ionicLoading) {

    $scope.goToEventLocalNotif = function(encodedId){
      var event = JSON.parse(encodedId);
      $state.go('app.event', { id : event.eventId });
    };

    $scope.goToEvent = function(notif){
      notificationService.readNotif(notif._id, function () {
        switch (notif.type) {
          case 'Event':
            $state.go('app.event', {id: notif.payload.id});
            break;
          case 'friends':
            break;
          case 'snapComment':
            $state.go('snapComment', {id: notif.payload.id});
            break;
        }
      });
    };

    notificationService.getAllNotif(function (data) {
      $scope.notifications = data.notifs;
    });

    $rootScope.$on('$cordovaLocalNotification:trigger', function(event, notification, state) {

      if (state === 'background'){

      } else {
        $cordovaLocalNotification.getAllTriggered().then(function(notifs){
          $scope.notifs = notifs;
          $cordovaVibration.vibrate(1000);
        });
      }
    });

    $rootScope.$on('$cordovaLocalNotification:click', function(event, notification, state) {
        $cordovaLocalNotification.getAllTriggered().then(function(notifs){
          $scope.notifs = notifs;
          $cordovaVibration.vibrate(1000);
        });
    });

    $rootScope.$on('pushNotification', function (event, notif) {
      $scope.$apply(function () {
        if (notif.additionalData.coldstart) {

        } else {
          $cordovaVibration.vibrate(1000);
        }
      })

    });

    var user = JSON.parse(window.localStorage.getItem('userProfile'));
    $scope.musicChoices = user.local.musicChoices ? user.local.musicChoices : [];
    $scope.eventChoices = user.local.eventChoices ? user.local.eventChoices : [];

    $scope.genres = TAG.genre;
    $scope.types = TAG.type;

    var ProfilePicture = null;

    $scope.uploadPicture = function () {
      var ProfilePicture = Utils.createUuid() + '.jpg';

      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 200,
        targetHeight: 200
      };

      $cordovaCamera.getPicture(options).then(function(imageUri) {
        $cordovaFileTransfer.upload(API_ENDPOINT.url + '/upload/avatar/', imageUri, {fileName : ProfilePicture}).then(function(result) {
          var user = JSON.parse(window.localStorage.getItem('userProfile'));
          user.local.pict.unshift(ProfilePicture);
          UserService.updateProfile(user, function (res) {
            window.localStorage.setItem('userProfile', JSON.stringify(user));
            $rootScope.$emit('image:update');
          });
        });
      }, function(err) {
        // error
      });
    };

    $scope.updateProfile = function (NewProfile) {
      $ionicLoading.show({
        template: 'Loading...'
      });
      var user = JSON.parse(window.localStorage.getItem('userProfile'));
      PluginService.getPosition(function (coords) {
        var position = {
          latitude: coords.latitude,
          longitude: coords.longitude
        };
        user.local.place.unshift(position);
        user.local.musicChoices = $scope.musicChoices;
        user.local.eventChoices = $scope.eventChoices;
        UserService.updateProfile(user, function (res) {
          user = res.data.msg;
          window.localStorage.setItem('userProfile', JSON.stringify(user));
          $ionicLoading.hide();
          $rootScope.$emit('enableMenu');
          $state.go('app.centrale');
        });
      });

    }
  })

  .controller('PreprofileCtrl', function ($rootScope, $scope,$ionicPlatform, $timeout, $ionicSideMenuDelegate, $ionicHistory, $cordovaImagePicker, $cordovaCamera, PluginService, TAG, UserService, $filter, $cordovaFileTransfer, API_ENDPOINT, Utils, $state, $ionicLoading) {
    $scope.musicChoices =  [];
    $scope.eventChoices =  [];

    $scope.genres = TAG.genre;
    $scope.types = TAG.type;

    $scope.updateProfile = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });
      var user = JSON.parse(window.localStorage.getItem('userProfile'));
        user.local.musicChoices = $scope.musicChoices;
        user.local.eventChoices = $scope.eventChoices;
        UserService.updateProfile(user, function (res) {
          user = res.data.msg;
          window.localStorage.setItem('tagg', 1);
          window.localStorage.setItem('userProfile', JSON.stringify(user));
          $ionicLoading.hide();
          $state.go('app.centrale');
        });
    }
  })

  .controller('SettingsCtrl', function ($rootScope, $scope, AuthService,MODE, $state, $cordovaAppRate, $cordovaGoogleAnalytics, $cordovaSocialSharing) {

    if (!MODE.debug){
      cordova.getAppVersion.getVersionNumber().then(function (version) {
        $scope.version = version;
      });
    } else {
      $scope.version = 'debug';
    }


    $scope.recommand = function () {
      if (!MODE.debug) {
        $cordovaGoogleAnalytics.trackEvent("Recommendation app");
        intercom.logEvent("Recommendation app");
      }
       if (ionic.Platform.isIOS()){
         window.open('itms-apps://itunes.apple.com/us/app/movment/id1112337276?ls=1&mt=8', '_blank');
       } else {
         window.open('https://play.google.com/store/apps/details?id=io.movmentAND.app', '_blank');
       }
    };

    $scope.share = function () {
      if (!MODE.debug) {
        $cordovaGoogleAnalytics.trackEvent("Share to a friend");
        intercom.logEvent("Share to a friend");
      }
      $cordovaSocialSharing
        .share("Voici une appli qui devrait te plaire ;-), télécharge movment et trouve la soirée qu'il te faut", "Message", null, "http://goo.gl/F8CGmb")
        .then(function(result) {
          // Success!
        }, function(err) {
          console.log(err);
          // An error occurred. Show a message to the user
        });
      //alert('Option non disponible pour le moment');
    };


    $scope.callus = function () {
      if (!MODE.debug) {
        $cordovaGoogleAnalytics.trackEvent("Call center");
        intercom.logEvent("Call center");
      }
      window.open('tel:+32489872455');
    };

    $scope.disconnect = function () {
      if (!MODE.debug) {
        window.localStorage.setItem('tagg', null);
        intercom.reset();
        intercom.registerUnidentifiedUser();
        $cordovaGoogleAnalytics.trackEvent("logout");
      }
      AuthService.logout();
      $rootScope.$emit('image:update');
      $state.go('Start');
    }
  })

  .controller('PreferencesCtrl', function ($scope, $stateParams, $ionicModal, $cordovaGoogleAnalytics, $timeout, $cordovaDialogs, $ionicLoading, $state, $ionicViewSwitcher, TAG) {

    var geocoder = new google.maps.Geocoder();
    var preferences = JSON.parse(window.localStorage.getItem('preferences'));
    var position = {};
    $scope.city = '';
    var city = null;
    $scope.filters = [];
    $scope.data = {};

    $scope.newCity = function () {
      city = $scope.data.city;
      geocoder.geocode({"address": city}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
          var location = results[0].geometry.location;
          position = {
            latitude: location.lat(),
            longitude: location.lng()
          };
        }
      });
    };
    $scope.musictype = TAG.genre;
    $scope.validate = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });

      preferences = {
        city: $scope.data.city,
        distance: $scope.data.distance,
        position: position
      };

          $cordovaGoogleAnalytics.trackEvent("Preferences", "click", preferences.city, preferences.distance);
          window.localStorage.setItem('preferences', JSON.stringify(preferences));
          $ionicLoading.hide();
          $ionicViewSwitcher.nextDirection("forward");
          $timeout(function () {
            $cordovaDialogs.alert('Pour le moment nous sommes uniquement disponible sur Bruxelles, d\'autres villes à venir prochainement 😎', 'Événement', 'OK')
              .then(function() {
                // callback success
              });
          }, 0);
          $state.go('app.centrale');
    };

  })

  .controller('UserCtrl', function ($ionicPush, $rootScope,$ionicHistory ,$ionicTabsDelegate, $scope,$timeout, AuthService,SqlLiteService, $state, $cordovaStatusbar, $cordovaGoogleAnalytics, $ionicPlatform) {
    var vm = this;
    var user = JSON.parse(window.localStorage.getItem('userProfile'));
    vm.user = user;
    $scope.hasNotif = false;
    vm.hide = true;
    vm.ios = ionic.Platform.isIOS();
    vm.backButton = false;

    vm.goBack = function () {
      $ionicHistory.goBack();
    };

    $scope.$on('$ionicView.enter', function (e, data) {
      var history = $ionicHistory.viewHistory();
      if (history.currentView.stateName != "app.centrale" && history.currentView.stateName != "app.organisers"
        && history.currentView.stateName != "app.wishlist" && history.currentView.stateName != "app.profile"){
        vm.backButton = true;
      } else {
        vm.backButton = false;
      }
      $scope.$apply(function () {
        if (data.stateName == 'app.event' || data.stateName.search(/app/i)){
          vm.hide = true;
        } else {
          vm.hide = false;
        }
        vm.title = data.title;
      });

    });

    $scope.$on('$ionicView.leave', function (e, data) {
          if (data.stateName == 'app.event' && data.stateName.search(/app/i) == 0){
            vm.hide = false;
          }
    });

    var tab =[];
    tab[0] = 'bar-yellow';
    tab[1] = 'bar-purple';
    tab[2] = 'bar-red';
    tab[3] = 'bar-green';

    vm.getTab = function () {
      return tab[$ionicTabsDelegate.selectedIndex()];
    };

    $scope.goNotif = function (notif) {
      $state.go('app.event',{ id : ionic.Platform.isIOS() ? notif.additionalData.eventId :notif.additionalData.custom.eventId });
    };
    $rootScope.$on('pushNotification', function (event, notif) {
      $scope.$apply(function () {
        $scope.hasNotif = true;
        $scope.notif = notif;
      });
      $timeout(function () {
        $scope.$apply(function () {
          $scope.hasNotif = false;
        });
      }, 5000);
    });

    $ionicPlatform.ready(function(){
      $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        user = JSON.parse(window.localStorage.getItem('userProfile'));
        if(user){
          $rootScope.$emit('image:update');
          $cordovaGoogleAnalytics.setUserId(user._id);
          intercom.registerIdentifiedUser({userId: user._id });
          window.mixpanel.identify(user._id);
        } else {
          intercom.registerUnidentifiedUser();
        }
        window.mixpanel.track('tracking view', { view : toState.name });
        $cordovaGoogleAnalytics.trackView(toState.name);
        intercom.logEvent("track view",  {
          viewName: toState.name
        });
      });
    });

    $rootScope.$on('image:update', function(){
      vm.user = JSON.parse(window.localStorage.getItem('userProfile'));
    });
  })

  .controller('MapCtrl', function ($scope, AuthService, $state, $stateParams, uiGmapGoogleMapApi, PluginService, $cordovaLaunchNavigator) {
    var vm = this;

    var data = $stateParams.position;
    var position1 = data.split('|');

    $scope.map = {center: {latitude: position1[0], longitude: position1[1]}, zoom: 17};

    $scope.marker = {
      id: 0,
      coords: {
        latitude: position1[0],
        longitude: position1[1]
      },
      click: function () {

        PluginService.getPosition(function (coords) {
          var position = {
            latitude: coords.latitude,
            longitude: coords.longitude
          };
          $cordovaLaunchNavigator.navigate([position1[0], position1[1]], null).then(function (success) {
            console.log(success);
          }, function (err) {
            console.log(err);
          });
        });
      },
      options: {
        labelContent: "Click here to see how to go",
        labelAnchor: "75 0",
        labelClass: "marker-labels"
      }
    };
    uiGmapGoogleMapApi.then(function (maps) {

    });

  })

  .controller('forgetCtrl', function ($scope, AuthService, $state) {

    $scope.data = {};

    $scope.back = function () {
     $state.go('connexion');
    };

    $scope.valid = function () {
      $scope.error = null;
      if ($scope.data.email){
      AuthService.forget($scope.data.email, function (result) {
         if (result.data.success) {
           $scope.success = result.data.msg;
         } else {
           $scope.error = result.data.msg;
         }
      })
      } else {
        $scope.error = 'Il manque l\'email ;-)';
      }
    };
  })

  .controller('snapCtrl', function ($rootScope, $scope, $ionicPopup,UserService, EventService, $ionicHistory, $cordovaCapture, $ionicModal,$ionicLoading, $cordovaFileTransfer, Utils, snapService, $state, API_ENDPOINT, AuthService) {
    var value = window.localStorage.getItem('prezSnap');
    var user = JSON.parse(window.localStorage.getItem('userProfile'));

    $ionicModal.fromTemplateUrl('templates/prezSnap.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.prez = modal;
      if (value != '1'){
        $scope.prez.show();
      }
    });

   /* $scope.updateProfile = function () {
      UserService.updateProfile({ local : $scope.local }, function (result) {
        $scope.infoCpt.hide();
        realVideo();
      });
    };*/

    $ionicModal.fromTemplateUrl('templates/infoCpt.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.infoCpt = modal;
    });


   /* $scope.infoCancel = function() {
      alert('without a name, a man is no one... 😉');
      $scope.infoCpt.hide();
      $scope.modal.hide();
    };
*/


    $scope.closePrez = function() {
      window.localStorage.setItem('prezSnap', 1);
      $scope.prez.hide();
    };

    $scope.exit = function () {
     $state.go('app.centrale');
    };

    $scope.go = function (id) {
      $state.go('app.event', {id : id});
    };

    $scope.data = {};

    $scope.getScrollPosition = function () {
      $rootScope.$emit('videoCheck');
    };

    function tonight() {
      EventService.getTonightEvent(function(data){
        $scope.events = data.events;
      });
    }

    $ionicModal.fromTemplateUrl('templates/snap.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    function check (){
      snapService.getVideos(function (data) {
        $scope.snaps = data.snaps;
      });
    }

    check();

    $scope.videoCapture = function () {
      $rootScope.$emit('PauseVideo');
      var options = { limit: 1, duration: 20 };
      tonight();
      $cordovaCapture.captureVideo(options).then(function (Video) {
        $scope.modal.show();
        $scope.presendVideo = Video[0].fullPath;
        var name =  Utils.createUuid() + '.mp4';
        $scope.send = function(event) {
          var position = angular.isDefined(event) ? { name: event.name, id: event._id } : {};
          $ionicLoading.show({
            template: 'Loading...'
          });
          $cordovaFileTransfer.upload(API_ENDPOINT.url + '/upload/snap/',
            Video[0].fullPath,
            {
              fileName: name,
              mimeType: 'video/quicktime',
              params: {
                description: $scope.data.description,
                eventId: angular.isDefined(position.id) ? position.id : null,
                name: angular.isDefined(position.name) ? position.name : null
              },
              headers: {
                Authorization: AuthService.getToken()
              }
            }).then(function (result) {
            check();
            $ionicLoading.hide();
            $scope.modal.hide();
          }, function (err) {
          });
        };

        $scope.cancel = function () {
          $scope.modal.hide();
        };

        $scope.exit = function () {
          $scope.modal.hide();
        };

      });
    };
  })
  .controller('commentSnapCtrl', function ($rootScope, $scope, $stateParams, $state, snapService, $ionicHistory) {

    var user = JSON.parse(window.localStorage.getItem('userProfile'));
    $rootScope.$emit('PauseVideo');
    function refresh(){
      snapService.getVideo($stateParams.id, function (data) {
        $scope.snap = data.snaps;
      });
    }
    refresh();

    $scope.go = function (id) {
      $state.go('app.event', {id : id});
    };

    $scope.exit = function () {
      $ionicHistory.goBack();
    };

      $scope.comment = function () {
        if (!angular.isUndefined($scope.message)){
        var data = {
        id : $scope.snap._id,
        comment: $scope.message,
        creator: {
          firstName: user.local.firstName ? user.local.firstName: user.facebook.firstName,
          lastName: user.local.lastName ? user.local.lastName : user.facebook.lastName,
          id: $scope.snap._id
        }
      };

      snapService.postComment(data, function (result) {
        $scope.message = '';
        refresh();
      });
      }
    };
  })
  .controller('organisersCtrl', function ($scope, organiserService, $ionicModal, followService) {

    $ionicModal.fromTemplateUrl('templates/modalOrganiser.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    followService.getFollowees(function (followed) {
        organiserService.getOrganisers(function (organisers) {
          $scope.organisers = organisers;
          $scope.followed = followed;
        });
      });

    $scope.close = function () {
      $scope.modal.hide();
    };

    $scope.see = function (obj) {
      $scope.selected = obj;
      $scope.modal.show();
    }

  });

