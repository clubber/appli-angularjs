/**
 * Created by mikeycrostar on 07/02/2016.
 */

angular.module('Movment')

  .factory('Utils', function(){
    'use strict';
    var service = {
      createUuid: createUuid,
      intoArr: intoArr
    };

    function createUuid(){
      function S4(){ return (((1+Math.random())*0x10000)|0).toString(16).substring(1); }
      return (S4() + S4() + '-' + S4() + '-4' + S4().substr(0,3) + '-' + S4() + '-' + S4() + S4() + S4()).toLowerCase();
    }

    function intoArr(res){
      var items = [];
      for (var x in res) {
        var item = res[x];
        if (item.name) {
          items.push(item);
        }
      }
      return items;
    }

    return service;
  });
