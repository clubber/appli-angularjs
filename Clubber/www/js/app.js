// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var db;
var isInit = 0;
angular.module('Movment', ['ionic', 'ionic.cloud', 'ngCordova', 'ngMessages', 'ionic-datepicker', 'angularReverseGeocode' ,'uiGmapgoogle-maps','ngAnimate', 'angular-google-analytics'])

  .run(function ($ionicPlatform, $ionicPush, MODE, $ionicNavBarDelegate, AuthService, notificationService, PluginService, $cordovaSplashscreen, $cordovaStatusbar, $rootScope, $state, $timeout, $cordovaGoogleAnalytics,SqlLiteService) {
    moment.locale('fr');

    $ionicPush.register().then(function(t) {
      return $ionicPush.saveToken(t);
    }).then(function(t) {
    });

      $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.$emit('PauseVideo');
        if (isInit == 0){
          $timeout(function () { navigator.splashscreen.hide(); }, 750);
          isInit = 1;
        }

        SqlLiteService.getToken(function (data) {
          if(!AuthService.isAuthenticated() && toState.name === 'app.centrale'){
            $state.go('Start');
            event.preventDefault();
          }

          if (AuthService.isAuthenticated() && toState.name == 'Start'){
            $state.go('app.centrale');
            event.preventDefault();
          }
        });
      });


    $rootScope.$on('login', function () {
      SqlLiteService.getToken(function (data) {
        var token = window.localStorage.getItem('YourTokenKey');
        if(!token && toState.name === 'app.centrale'){
          $state.go('Start');
        }
      });
    });


    $ionicPlatform.ready(function () {
      $timeout(function () { navigator.splashscreen.hide(); }, 3000);
      if (!MODE.debug) {
        window.mixpanel.init('f323b98b8acd278f2c5da73ed1705c54', function (success) {
          console.log(success);
        }, function (error) {
          console.log(error);
        });
        var push = PushNotification.init({
          android: {
            senderID: "608995360280",
            icon: 'icon'
          },
          ios: {
            alert: "true",
            badge: true,
            sound: 'true',
            clearBadge: true
          }
        });

        push.on('registration', function (data) {
          mixpanel.people.setPushId(data.registrationId);
          notificationService.setPushToken(data.registrationId);
        });

        push.on('notification', function(data){
          if (!data.additionalData.payload.coldstart){
            switch (data.additionalData.payload.type) {
              case 'Event':
                $state.go('app.event', {id: data.additionalData.payload.id});
                break;
              case 'friends':
                break;
              case 'snapComment':
                $state.go('snapComment', {id: data.additionalData.payload.id});
                break;
            }
          } else {
            $rootScope.$emit('pushNotification', data);
          }

          push.finish(function() {
            console.log("processing of push data is finished");
          });
        });
      }

      $cordovaGoogleAnalytics.startTrackerWithId('UA-77506799-1');
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $ionicCloudProvider, $httpProvider, $cordovaAppRateProvider, $ionicConfigProvider, ionicDatePickerProvider, uiGmapGoogleMapApiProvider, AnalyticsProvider) {
    $ionicCloudProvider.init({
      "core": {
        "app_id": "9a297abd"
      },
      "push": {
        "sender_id": "608995360280",
        "pluginConfig": {
          "ios": {
            "badge": true,
            "sound": true,
            "clearBadge": true
          },
          "android": {
            "icon": "http://www.organisateur.movment.io/img/logo-footer.png"
          }
        }
      }
    });


    document.addEventListener("deviceready", function () {
      db = window.sqlitePlugin.openDatabase({name: 'Movment.db', location: 'default'}, function(success){

      }, function (err) {
        alert('Not enough memory');
      });

       $cordovaAppRateProvider.setPreferences({
         language: 'en',
         appName: 'Movment',
         usesUntilPrompt: 3,
         openStoreInApp: true,
         iosURL: '1112337276',
         androidURL: 'market://details?id=io.movmentAPP.app'
       });
    });

    uiGmapGoogleMapApiProvider.configure({
      //    key: 'your api key',
      v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    });


    var datePickerObj = {
      inputDate: new Date(),
      setLabel: 'Sélectionner',
      todayLabel: 'Aujourd\'hui',
      closeLabel: 'Fermer',
      mondayFirst: true, //
      weeksList: ["D", "L", "M", "M", "J", "V", "S"],
      monthsList: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"],
      templateType: 'popup',
      from: new Date(),
      showTodayButton: true,
      dateFormat: 'dd MMMM yyyy',
      closeOnSelect: true
    };

    ionicDatePickerProvider.configDatePicker(datePickerObj);

    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.scrolling.jsScrolling(true);
    $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left');
    $httpProvider.defaults.headers.common = {'X-Requested-With': 'XMLHttpRequest'};
    $stateProvider
      .state('Start', {
        url: '/start',
        templateUrl: 'templates/start.html',
        controller: 'StartCtrl'
      })
      .state('forget', {
        url: '/forget',
            templateUrl: 'templates/forget.html',
            controller: 'forgetCtrl'
      })
      .state('connexion', {
        url: '/connexion',
        templateUrl: 'templates/connexion.html',
        controller: 'ConnexionCtrl'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'templates/subcribe.html',
        controller: 'SignupCtrl'
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/tab.html',
        controller: 'AppCtrl'
      })
      .state('app.centrale', {
        url: '/centrale',
        views: {
          'home-tab': {
            templateUrl: 'templates/centrale.html',
            controller: 'CentraleCtrl'
          }
        }
      })
      .state('app.event', {
        url: '/event/:id',
        views: {
          'home-tab': {
            templateUrl: 'templates/event.html',
            controller: 'EventCtrl'
          }
        }
      }).state('app.about', {
        url: '/about',
        views: {
          'profile-tab': {
            templateUrl: 'templates/menu/about.html'
          }
        }
      })
      .state('app.nights', {
        url: '/nights',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/nights.html',
            controller: 'nightsCtrl'
          }
        }
      })
      .state('app.clubbing', {
        url: '/clubbing',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/clubbing.html',
            controller: 'clubbingCtrl'
          }
        }
      })

      .state('app.tomorrow', {
        url: '/tomorrow',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/tomorrow.html',
            controller: 'tomorrowCtrl'
          }
        }
      })

      .state('app.weekend', {
        url: '/weekend',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/weekend.html',
            controller: 'weekendCtrl'
          }
        }
      })

      .state('app.afterwork', {
        url: '/afterwork',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/afterwork.html',
            controller: 'afterworkCtrl'
          }
        }
      })

      .state('app.karaoke', {
        url: '/karaoke',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/karaoke.html',
            controller: 'karaokeCtrl'
          }
        }
      })
      .state('app.pubcrawl', {
        url: '/pubcrawl',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/pubcrawl.html',
            controller: 'pubcrawlCtrl'
          }
        }
      })

      .state('app.booking', {
        url: '/booking',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/booking.html',
            controller: 'bookingCtrl'
          }
        }
      })

      .state('app.Clubs', {
        url: '/Clubs',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/soon.html',
            controller: 'ClubsCtrl'
          }
        }
      })

      .state('app.concerts', {
        url: '/concert',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/concert.html',
            controller: 'ConcertCtrl'
          }
        }
      })

      .state('app.houseparty', {
        url: '/houseparty',
        views: {
          'home-tab': {
            templateUrl: 'templates/tules/festival.html',
            controller: 'HousePartyCtrl'
          }
        }
      })

      .state('app.wishlist', {
          url: '/favoris',
          views: {
            'home-tab': {
              templateUrl: 'templates/tules/wishlist.html',
              controller: 'FavCtrl'
            }
          }
        }
      )
      .state('app.after', {
          url: '/after',
          views: {
            'home-tab': {
              templateUrl: 'templates/tules/after.html',
              controller: 'AfterCtrl'
            }
          }
        }
      )

      .state('app.profile', {
          url: '/profile',
          views: {
            'profile-tab': {
              templateUrl: 'templates/menu/myprofile.html',
              controller: 'ProfileCtrl'
            }
          }
        }
      )

      .state('preprofile', {
          url: '/preprofile',
          templateUrl: 'templates/preprofile.html',
          controller: 'PreprofileCtrl'
        }
      )

      .state('app.preferences', {
          url: '/preferences',
          views: {
            'profile-tab': {
              templateUrl: 'templates/preprofile.html',
              controller: 'PreprofileCtrl'
            }
          }
        }
      )
      .state('app.settings', {
          url: '/settings',
          views: {
            'profile-tab': {
              templateUrl: 'templates/menu/mysettings.html',
              controller: 'SettingsCtrl'
            }
          }
        }
      )
      .state('app.friends', {
          url: '/friends',
          views: {
            'friends-tab': {
              templateUrl: 'templates/friends.html',
              controller: 'friendsCtrl'
            }
          }
        }
      )
      .state('app.map', {
          url: '/map/:position',
          views: {
            'home-tab': {
              templateUrl: 'templates/map.html',
              controller: 'MapCtrl'
            }
          }
        })
      .state('app.organisers', {
        url: '/organisers',
        views: {
          'orga-tab':{
            templateUrl: 'templates/organiser.html',
            controller: 'organisersCtrl'
          }
        }
      })
      .state('snap', {
        url: '/snap',
        templateUrl: 'templates/videoline.html',
        controller: 'snapCtrl'
      })
      .state('snapComment', {
        url: '/commentSnap/:id',
        templateUrl: 'templates/snap/comment.html',
        controller: 'commentSnapCtrl'
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise(function ($injector, $location) {
      var $state = $injector.get("$state");
      $state.go('app.centrale');
    });
  })
  .provider('$exceptionHandler', {
    $get: function(errorLogService) {
      return(errorLogService);
  }})
  .constant('MODE', {
    debug: 0
  })
  .constant('API_ENDPOINT', {
    url: 'http://movment.io:8000'
    //  For a simulator use: url: 'http://127.0.0.1:8080/api'
  })
  .constant('TAG', {
    genre: ['Rock', 'Pop' , 'Jazz' , 'Blues',  'Hip-hop' , 'Soul' , 'Electro' , 'Métal', 'Afro' , 'Latino' , 'Généraliste'],
    date: ['Tonight', 'Tomorrow', 'Weekend', 'Date'],
    price: ['Free', '€', '€€'],
    type: ['Afterwork', 'Clubbing', 'After', 'Pubcrawl', 'House party',  'Concert', 'Student party'],
    weekend: ['Friday', 'Saturday', 'Sunday']
  });

