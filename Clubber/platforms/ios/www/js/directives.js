/**
 * Created by mikeycrostar on 22/01/2016.
 */

angular.module('Movment')

  .directive('userImageCircle', function (API_ENDPOINT) {
    return {
      restrict: 'E',
      replace: true,
      template: '<img class="vm.class" ng-src="{{ vm.image }}" />',
      scope: {
        user: '=',
        class: '@'
      },
      controller: function ($scope) {
        var vm = this;
        vm.class = $scope.class;
        $scope.$watch('user', function (newValue, oldValue) {
          if (angular.isDefined(newValue)) {
            var user = newValue;
            if (angular.isDefined(user.local.pict[0]) && user.local.pict[0] != null) {
              vm.image = API_ENDPOINT.url + '/avatar/' + user.local.pict[0];
            } else if (angular.isDefined(user.facebook)
              && angular.isDefined(user.facebook.photo[0])) {
              vm.image = user.facebook.photo[0].value;
            } else {
              vm.image = 'img/avatar.png';
            }
          }
        });
      },
      controllerAs: 'vm'
    };
  })

  .directive('userImageSquare', function (API_ENDPOINT) {
    return {
      restrict: 'E',
      template: '<img class="circle" style="{{ vm.style }}" ng-src="{{ vm.image }}" />',
      scope: {
        user: '=',
        class: '=',
        style: '@'
      },
      controller: function ($scope) {
        var vm = this;
        vm.style = $scope.style;
        $scope.$watch('user', function (newValue, oldValue) {
          if (angular.isDefined(newValue)) {
            var user = newValue;
            if (angular.isDefined(user.local.pict[0]) && user.local.pict[0] != null) {
              vm.image = API_ENDPOINT.url + '/avatar/' + user.local.pict[0];
            } else if (angular.isDefined(user.facebook)
              && angular.isDefined(user.facebook.photo[0])) {
              vm.image = user.facebook.photo[0].value;
            } else {
              vm.image = 'img/avatar.png';
            }
          }
        });
      },
      controllerAs: 'vm'
    };
  })

  .directive('filterButton', function (MODE) {
    return {
      scope: {
        array: '=',
        value: '='
      },
      replace: true,
      restrict: 'E',
      controller: function ($scope, ionicDatePicker, $ionicLoading, $timeout) {
        var user = JSON.parse(window.localStorage.getItem('userProfile'));
        $scope.user = user;

        $scope.addFilter = function () {
          $scope.array.push($scope.value);
        };

        $scope.removeFilter = function () {
          var flt = $scope.array;

          for (var i = 0; i < flt.length; i++) {
            if (flt[i] == $scope.value) {
              flt.splice(i, 1);
            }
          }
        };

        $scope.openDatePicker = function (cb) {
          var ipObj1 = {
            callback: function (val) {
              cb(val);
            }
          };
          ionicDatePicker.openDatePicker(ipObj1);
        };

        $scope.loading = function () {
          $ionicLoading.show({
            template: '<ion-spinner icon="lines"></ion-spinner>'
          });
        };

        $scope.hide = function() {
          $timeout(function () {
            $ionicLoading.hide();
          }, 500);
        }
      },
      link: function (scope, elem, attrs, ctrl) {
        var time = null;
        elem.html('<button class="button-filter">' + scope.value + '</button>');

        var enable = 0;
        if (searchTag(scope.user, scope.value) && enable == 1){
          elem.children('button').addClass('active');
        }

        elem.bind('click', function () {
          scope.loading();
          if (!MODE.debug){
            window.mixpanel.track('filter', {  filterName:scope.value  });
          }
          if (elem.children('button').hasClass('active')) {
            if (scope.value == 'Date') {
              scope.$apply(function () {
                scope.removeFilter(time);
                time = null;
              });
              elem.children('button').removeClass('active');
            } else {
              scope.$apply(function () {
                scope.removeFilter();
              });
              elem.children('button').removeClass('active');
            }
          } else {
            if (scope.value == 'Date') {
              scope.openDatePicker(function (date) {
                time = date;
                scope.addFilter(date);
              });

            } else {
            scope.$apply(function () {
              scope.addFilter();
            });
            }
            elem.children('button').addClass('active');
          }
          scope.hide();
        });
      }
    }
  })
  .directive('urlBackground', function () {
    return {
      scope : {
        urlBg: '='
      },
      restrict: 'AE',
      controller :function ($scope, $element) {
        $scope.$watch('urlBg', function(){
          $element.css({
            'background-color': '#690B07',
            'background': 'linear-gradient(rgba(0,0,0, 0.50),rgba( 0,0,0,0.50)), url(http://62.210.108.100:8000/eventImage/' + encodeURIComponent($scope.urlBg[0]) + ') no-repeat center center',
            '-webkit-background-size': 'cover',
            '-moz-background-size': 'cover',
            '-o-background-size': 'cover',
            'background-size': 'cover'
          })
        });
      }
    }
  })
  .directive('wishList', function () {
    return {
      restrict: 'E',
      replace: true,
      template: '<a class="wishlist-button" ng-click="vm.action()"><i class="ion {{ vm.html }}"></i></a>',
      scope: {
        event: '='
      },
      controller: function ($scope, $rootScope, MODE) {
        var vm = this;
        var exist = 0;
        var wishlist = [];

        function check() {
          exist = 0;
          wishlist = JSON.parse(window.localStorage.getItem('Wishlist')) || [];

          for (var i = 0; i < wishlist.length; i++) {
            if (wishlist[i]._id == $scope.event._id) {
              exist = 1;
            }
          }

          if (exist) {
            vm.html = 'ion-ios-heart';
            vm.action = rmFavoris;
          } else {
            vm.html = 'ion-ios-heart-outline';
            vm.action = addFavoris;
          }
        }

        check();

        function addFavoris() {
          if (!MODE.debug){
            intercom.logEvent("Fav event", {
              dateAtAction: moment(),
              eventName: $scope.event.name,
              eventId: $scope.event._id
            });
          }

          wishlist.push($scope.event);
          window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
          $rootScope.$emit('Wishlist');
          check();
        }

        function rmFavoris() {
          for (var i = 0; i < wishlist.length; i++) {
            if (wishlist[i]._id == $scope.event._id) {
              wishlist.splice(i, 1);
            }
          }
          window.localStorage.setItem('Wishlist', JSON.stringify(wishlist));
          $rootScope.$emit('Wishlist');
          check();
        }
      },
      controllerAs: 'vm'
    };
  })
  .directive('reverseGeocodeAdress', function () {
    return {
      restrict: 'E',
      template: '<div></div>',
      link: function (scope, element, attrs) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(attrs.lat, attrs.lng);
        geocoder.geocode({'latLng': latlng}, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              element.text(results[1].formatted_address);
            } else {
              element.text('Location not found');
            }
          } else {
            element.text('Geocoder failed due to: ' + status);
          }
        });
      },
      replace: true
    }
  })

  .directive('urlBackgroundCentrale', function () {
    return function (scope, element, attrs) {
      element.css({
        'background-color': '#690B07',
        'background': 'linear-gradient(rgba(105,11,7, 0.20),rgba(105,11,7, 0.20)), url(img/' + encodeURIComponent(attrs.urlbackground) + ') no-repeat center center',
        '-webkit-background-size': 'cover',
        '-moz-background-size': 'cover',
        '-o-background-size': 'cover',
        'background-size': 'cover'
      })
    }
  })
  .directive('videoPlayer', function () {
    return {
      restrict: 'E',
      template: '<video class="videoPlayer" preload="auto" width="{{ vm.viewport.width }}" loop webkit-playsinline="webkit-playsinline" ng-src="{{ trustSrc(vm.video) }}" /><div class="mute ion-ios-volume-low"></div></video>',
      scope: {
        video: '='
      },
      controller: function ($rootScope, $scope, $timeout, API_ENDPOINT, $sce, $element) {
        var video = $element.children('video')[0];
        var mute = $element.children('div');
        video.muted = true;

        function checkSound () {
          if(mute.hasClass('ion-ios-volume-low')){
            mute.removeClass('ion-ios-volume-low');
            mute.addClass('ion-ios-volume-high');
            video.muted = false;
          } else {
            mute.removeClass('ion-ios-volume-high');
            mute.addClass('ion-ios-volume-low');
            video.muted = true;
          }
        }

        $rootScope.$on('PauseVideo', function () {
          video.pause();
        });

        $element.bind('click', function(){
          checkSound();
        });

        function checkVisiblity(){
          if ($element.visible()){
            video.play();
          } else {
            video.pause();
            video.muted = true;
            mute.removeClass('ion-ios-volume-high');
            mute.addClass('ion-ios-volume-low');
          }
        }
        $timeout(function() {
          checkVisiblity();
        }, 500);

        $rootScope.$on('videoCheck', function(){
          checkVisiblity();
        });

        $scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
        };
        var vm = this;
        vm.video = API_ENDPOINT.url + '/video/' + $scope.video;

        var viewport = {
          width  : $(window).width(),
          height : $(window).height()
        };

        vm.viewport = viewport;
      },
      controllerAs: 'vm',
      link: function (scope, el, at) {

      }
    };
  })

  .directive('eventItem', function () {

    return {
      restrict: 'E',
      templateUrl:'./templates/tules/item/d_event.html',
      scope: true
    }
  })
  .directive('likeData', function() {

    return {
      replace: true,
      template: '<i ng-click="vm.action()" style="font-size: 40px; color: #fcb415;font-weight: bolder;" class="ion {{ vm.icon }}"></i>',
      restrict: 'E',
      scope : {
        videoId : '@',
        likesList: '='
      },
      controller : function ($scope, snapService) {
        var vm = this;
        var user = JSON.parse(window.localStorage.getItem('userProfile'));
        var exist = 0;
        var queryingFor = '';
        vm.icon = 'ion-ios-heart-outline';

        function check() {
          exist = 0;
          for (var i = 0; i < $scope.likesList.length; i++) {
            if ($scope.likesList[i]._id == user._id) {
              exist = 1;
            }
          }
          if (exist){
            queryingFor = 'dislike';
            vm.icon = 'ion-ios-heart';
          } else {
            queryingFor = 'like';
            vm.icon = 'ion-ios-heart-outline';
          }
        }

        vm.action = function (){
          snapService.likeVideo($scope.videoId, { queryingFor : queryingFor }, function (data) {
            if (!exist){
              vm.icon = 'ion-ios-heart';
              exist = 1;
            } else {
              vm.icon = 'ion-ios-heart-outline';
              exist = 0;
            }
          })
        }
      },
      controllerAs: 'vm'
    }
  })
  .directive('hideTabs', function($rootScope, $state) {
    return {
      restrict: 'A',
      link: function($scope, $el) {
        if (!ionic.Platform.isIOS()){
          $rootScope.hideTabs = ' tabs-item-hide';

          $scope.$on('$destroy', function() {
            $rootScope.hideTabs = '';
          });
        }
      }
    };
  })
  .directive('hideTabsForce', function($rootScope) {
    return {
      restrict: 'A',
      link: function($scope, $el) {

          $rootScope.hideTabs = ' tabs-item-hide';
          $scope.$on('$destroy', function() {
            $rootScope.hideTabs = '';
          });

      }
    };
  })
  .directive('follow',  function () {
  return {
    restrict: 'AE',
    replace: true,
    scope: {
      organiserId: '=',
      listFollowing: '='
    },
    template: '<button class="button followBtn {{ vm.active }}" ng-click="vm.action()">{{ vm.text }} <i ng-show="vm.check" class="ion ion-checkmark-round"></i></button>',
    controller: function ($scope, followService, $ionicPopup) {
      var vm = this;

      function check(followed, id) {
        for (var i = 0; i < followed.length; i++){
          if (id == followed[i]){
            return true;
          }
        }
        return false;
      }

      function follow () {
        followService.followUser($scope.organiserId, function () {
          intercom.logEvent("follow", {
            follow: $scope.organiserId
          });
          vm.text = 'Following ';
          vm.active = 'active';
          vm.check = true;
          vm.action = unfollow;
        })
      }

      function unfollow() {
        var confirmPopup = $ionicPopup.confirm({
          //title: 'Unfollow this organiser',
          template: 'Do you really want to unsubcribe updates from this organizer ?',
          okType: 'red'
        });

        confirmPopup.then(function(res) {
          if(res) {
            followService.unfollowUser($scope.organiserId, function () {
              intercom.logEvent("unfollow", {
                unfollow: $scope.organiserId
              });
              vm.text = 'Follow';
              vm.active = '';
              vm.check = false;
              vm.action = follow;
            });
          } else {
            console.log('You are not sure');
          }
        });
      }

      $scope.$watchGroup(['listFollowing', 'organiserId'], function () {
          if (check($scope.listFollowing, $scope.organiserId)){
            vm.text = 'Following ';
            vm.active = 'active';
            vm.check = true;
            vm.action = unfollow;
          } else {
            vm.text = 'Follow';
            vm.action = follow;
            vm.active = '';
            vm.check = false;
          }
      });

    },
    controllerAs: 'vm'
  }
});


function searchTag(user, name){
    var music = user && user.local && user.local.musicChoices ? user.local.musicChoices : [];
    var event = user && user.local && user.local.eventChoices ? user.local.eventChoices : [];
    for (var i = 0; i < music.length; i++){
      if (music[i] == name){
        return 1;
      }
    }

    for (var y = 0; y < event.length; y++) {
      if (event[y] == name){
        return 1;
      }
    }
  return 0;
}
