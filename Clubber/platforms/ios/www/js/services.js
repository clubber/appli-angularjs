/**
 * Created by mikeycrostar on 09/01/2016.
 */

angular.module("Movment")
  .factory('stacktraceService', function () {
    return ({
        print: StackTrace
    })
  })

.service('AuthService',function ($http, $q, API_ENDPOINT, SqlLiteService, $timeout) {
  var LOCAL_TOKEN_KEY = 'YourTokenKey';
  var isAuthenticated = false;
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function getToken(){
    return authToken;
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;

    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }

  function destroyUserCredentials() {
    SqlLiteService.disconnect(function (result) {
      if(result.success){
      authToken = undefined;
      isAuthenticated = false;
      $http.defaults.headers.common.Authorization = undefined;
      window.localStorage.removeItem(LOCAL_TOKEN_KEY);
      window.localStorage.removeItem('userProfile');
      window.localStorage.removeItem('location');
      return true;
    }
    return false;
  });
  }

  var register = function(user) {
    return $http.post(API_ENDPOINT.url + '/signup', user);

  };

  var login = function (user, cb) {
      $http.post(API_ENDPOINT.url + '/login', user).then(function(result) {
          storeUserCredentials(result.data.token);
          getProfile(function (user) {
          if (user){
            window.localStorage.setItem('userProfile', JSON.stringify(user));
            SqlLiteService.login(user, result.data.token, function (result) {
            });
            intercom.registerIdentifiedUser({userId: user._id, email: user.local.email });
            cb(result);
          } else {
            cb({message: 'Something wrong happened'});
          }
        });
      }, function (error) {
        cb(error);
      });

  };

  var FBlogin = function(token, cb) {
    $http.post(API_ENDPOINT.url + '/auth/fb', {token: token}).then(function (result) {
      storeUserCredentials(result.data.token);
      window.localStorage.setItem('userProfile', JSON.stringify(result.data.user));
      SqlLiteService.login(result.data.user, result.data.token, function (result) {
        cb(result);
      });
    })
  };

  var FBupdate = function (cb) {
    $http.get(API_ENDPOINT.url + '/fb/update').then(function (res) {
      cb(res.data);
    }, function (error) {
      cb(error);
    })
  };

  var getProfile = function (cb) {
    $http.get(API_ENDPOINT.url + '/profile').then(function (res) {
      cb(res.data.user);
    }, function (error) {
      cb(null);
    })
  };

  function forget(email, cb) {
    $http.post(API_ENDPOINT.url + '/forgot', {email : email}).then(function (data) {
      cb(data);
    }, function (err) {
      cb(err);
    })
  }

  var logout = function() {
    destroyUserCredentials();
  };

  loadUserCredentials();

  return {
    login: login,
    FBlogin: FBlogin,
    FBupdate: FBupdate,
    getProfile: getProfile,
    register: register,
    forget: forget,
    logout: logout,
    getToken:getToken,
    isAuthenticated: function() {return isAuthenticated;}
  };

})
  .service('EventService', function ($q, $http, API_ENDPOINT) {


    var getEventType = function (type) {
      return $http.post(API_ENDPOINT.url + '/event/type/', {type: type})
    };

    var getTonightEvent = function (cb) {
      $http.get(API_ENDPOINT.url + '/tonight').then(function (res) {
        cb(res.data);
      });
    };

    var getTomorrowEvent = function (cb) {
      $http.get(API_ENDPOINT.url + '/tomorrow').then(function (res) {
        cb(res.data);
      });
    };

    var getWeekendEvent = function () {
      return $http.get(API_ENDPOINT.url + '/wk');
    };

    var getEvents = function (cb) {
      $http.get(API_ENDPOINT.url + '/events').then(function (res) {
        cb(res.data);
      });
    };

    var getEventById = function (params, cb) {
      $http.get(API_ENDPOINT.url + '/event/' + params).then(function (data) {
        cb(data.data);
      })
    };

    var setGoing = function (data, cb) {

      var send = {
        value: data.value,
        eventId: data.eventId
      };

      $http.post(API_ENDPOINT.url + '/event/interest/', send).then(function (result) {
        cb(result);
      });
    };


    var rmGoing = function (data, cb) {
      $http.post(API_ENDPOINT.url + '/event/desinterest/', data).then(function (result) {
        cb(result.data);
      })

    };


    return {
      getTonightEvent:getTonightEvent,
      getTomorrowEvent:getTomorrowEvent,
      getWeekendEvent: getWeekendEvent,
      getEventType: getEventType,
      getEventById:getEventById,
      getEvents:getEvents,
      setGoing: setGoing,
      rmGoing: rmGoing
    }

})
  .service('PluginService', function ($cordovaGeolocation, $ionicPlatform) {

    var getPosition = function (cb){
        $cordovaGeolocation.getCurrentPosition({
          enableHighAccuracy: true,
          timeout: 2000
        }).then(function (position) {
          cb(position.coords);
        }, function (err) {
          cb(err);
        });
    };

    return {
      getPosition:getPosition
    };
  })
  .service('UserService', function($http, API_ENDPOINT){

    var updateProfile = function (data, cb) {
      $http.post(API_ENDPOINT.url + '/user/update', { user: data } ).then(function (result) {
          cb(result);
      });
    };

    var getUserActivities = function (cb) {
      $http.get(API_ENDPOINT.url + '/user/activities').then(function(data){
          cb(data);
      });
    };


    return {
      updateProfile:updateProfile,
      getUserActivities: getUserActivities
    }

  })
  .service('snapService', function ($http, API_ENDPOINT) {

    var postVideo = function () {
        $http.post(API_ENDPOINT)
    };

    var postComment = function (data, cb) {
        $http.post(API_ENDPOINT.url + '/comment/snap/' + data.id, data).then(function (result) {
          cb(result.data);
        })
    };


    var getVideos = function (cb) {
        $http.get(API_ENDPOINT.url + '/snaps').then(function (result) {
          cb(result.data);
        })
    };

    var getVideo = function (id, cb) {
        $http.get(API_ENDPOINT.url + '/snap/' + id ).then(function (result) {
          cb(result.data);
        })
    };

    var likeVideo = function(id, data, cb){
      $http.post(API_ENDPOINT.url + '/snap/like/' + id, data).then(function (result){
        cb(result.data);
      });
    };

    return {
      postVideo: postVideo,
      postComment: postComment,
      getVideos: getVideos,
      getVideo: getVideo,
      likeVideo: likeVideo
    }
  })

.factory('SqlLiteService', function ($ionicPlatform, MODE) {

    var init = function () {
      if(!MODE.debug) {
        db.executeSql('CREATE TABLE IF NOT EXISTS login (id integer primary key, userId text, user text, Token text)');
      }
    };


    var login = function (user, Token, cb) {
      if(!MODE.debug) {
      $ionicPlatform.ready(function () {
        db.executeSql('DROP TABLE IF EXISTS login');
        init();
        db.executeSql('SELECT Token, user, userId FROM login WHERE userId = ?',[user._id], function (ob) {
          if(ob.rows.length == 0){
            db.executeSql('INSERT INTO login (userId, user, Token) VALUES (?, ?, ?)', [user._id, JSON.stringify(user), Token], function (tx) {
              cb({success: true, result:tx.rows.item(0)})
            }, function (err) {
              console.log(err);
            });
          } else {
            cb({success: false});
          }
        }, function (err) {
          console.log(err);
        })
      });
      } else {
        cb({success: true, result:JSON.parse(window.localStorage.getItem('userProfile'))});
      }
    };

  var disconnect = function (cb) {
    if (!MODE.debug){
    var token = window.localStorage.getItem('YourTokenKey');
    db.executeSql('DELETE FROM login WHERE Token = "'+  token +'"',[], function (tx) {
      cb({success: true });
    }, function(error){
      cb({success: false });
    });
    } else {
      cb({success: true });
    }
  };

  var getToken = function (cb) {
    if (!MODE.debug){
    $ionicPlatform.ready(function () {
      init();
      db.executeSql('SELECT * FROM login',[], function (ob) {
        if (ob.rows.length == 0){
          cb({success: false});
        } else {
          window.localStorage.setItem('YourTokenKey', ob.rows.item(0).Token);
          window.localStorage.setItem('userProfile', ob.rows.item(0).user);
          cb({success: true, data: ob.rows.item(0)});
        }
      }, function(err){
        console.log(err);
      })
    });
    } else {
      cb({success: true, data: window.localStorage.getItem('YourTokenKey')});
    }
  };

  return {
    login: login,
    disconnect: disconnect,
    getToken: getToken
  }

})
  .service('organiserService', function ($http, API_ENDPOINT) {

  var getOrganisers = function (cb) {
    $http.get(API_ENDPOINT.url + '/organisers').then(function (result) {
      cb(result.data.msg);
    })
  };

  return {
    getOrganisers: getOrganisers
  }

})

  .service('followService', function ($http, API_ENDPOINT) {

  var getFollowers = function (cb) {
    $http.get(API_ENDPOINT.url + '/people/follower').then(function (result) {
        cb(result.data.user);
    })
  };

  var getFollowees = function (cb) {
    $http.get(API_ENDPOINT.url + '/people/followees').then(function (result) {
      cb(result.data.user);
    })
  };

  var followUser = function (id, cb) {
    $http.post(API_ENDPOINT.url + '/people/follow/' + id).then(function (result) {
      cb(result.data.user);
    })

  };

  var unfollowUser = function (id, cb) {
    $http.post(API_ENDPOINT.url + '/people/unfollow/' + id).then(function (result) {
      cb(result.data.user);
    })
  };


    return {
      getFollowers: getFollowers,
      getFollowees: getFollowees,
      followUser: followUser,
      unfollowUser: unfollowUser
    }
  })


.service('notificationService', function ($http, API_ENDPOINT) {
  var pushToken;

  function setPushToken(tok) {
    pushToken = tok;
  }

  function getAllNotif(cb) {
    $http.get(API_ENDPOINT.url + '/push/notification').then(function (result) {
      cb(result.data);
    }, function (err) {
      console.log(err);
    });
  }


    function pushSub(type, userId) {
      if (angular.isDefined(pushToken)) {
        $http.post(API_ENDPOINT.url + '/push/sub', {token: pushToken, type: type, userId: userId});
      }
    }

  function readNotif(id, cb){
    $http.post(API_ENDPOINT.url + '/notif/read/' + id).then(function (result) {
      cb(result);
    });
  }
  return {
    setPushToken:setPushToken,
    pushSub: pushSub,
    getAllNotif: getAllNotif,
    readNotif: readNotif
  }
})
  .factory('errorLogService', function ($log, $window, stacktraceService, API_ENDPOINT, MODE) {
    function log(exception, cause) {
      $log.error.apply($log, arguments);

      try {
        if (!MODE.debug){
          var errorMessage = exception.toString();
          stacktraceService.print.fromError(exception).then(function (stackTrace) {
            $.ajax({
              type: "POST",
              url: API_ENDPOINT.url + '/error',
              contentType: "application/json",
              data: angular.toJson({
                errorUrl: $window.location.href,
                errorMessage: errorMessage,
                version: ionic.Platform.version(),
                stackTrace: stackTrace,
                cause: (cause || "")
              })
            });
          });
        }
      } catch (loggingError) {
        $log.warn("Error logging failed");
        $log.log(loggingError);
      }
    }

    return log;

  })
 .service('guestListService', function ($log, $window, stacktraceService, API_ENDPOINT, MODE) {
    function log(exception, cause) {
      $log.error.apply($log, arguments);

      try {
        if (!MODE.debug){
          var errorMessage = exception.toString();
          stacktraceService.print.fromError(exception).then(function (stackTrace) {
            $.ajax({
              type: "POST",
              url: API_ENDPOINT.url + '/error',
              contentType: "application/json",
              data: angular.toJson({
                errorUrl: $window.location.href,
                errorMessage: errorMessage,
                version: ionic.Platform.version(),
                stackTrace: stackTrace,
                cause: (cause || "")
              })
            });
          });
        }
      } catch (loggingError) {
        $log.warn("Error logging failed");
        $log.log(loggingError);
      }
    }

    return log;

  });

