/**
 * Created by mikeycrostar on 26/01/2016.
 */

angular.module('Movment')
  .filter('Type', function () {
    function distance(lon1, lat1, lon2, lat2) {

      function rad(toRad) {
        return toRad * Math.PI / 180;
      }

      var R = 6371; // Radius of the earth in km
      var dLat = rad((lat2 - lat1));  // Javascript functions in radians
      var dLon = rad((lon2 - lon1));
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c; // Distance in km
      return d;
    }

    function check_array(event, filters) {
      var t = 0;

      for (var y = 0; y < filters.length; y++) {
        if (
          monthsFilter(event.dateStart, filters[y]) ||
          dateFilter(event.dateStart, filters[y]) ||
          dayFilter(event.dateStart, filters[y]) ||
          weekendFilter(event.dateStart, filters[y]) ||
          randomDate(event.dateStart, filters[y]) ||
          moneyFilter(event.price_m, filters[y])) {
          t++;
        }
      }

      if (t >= filters.length) {
        return true;
      } else {
        return false;
      }
    }

    function dateFilter(date, filter) {

      if (angular.isDefined(filter) && filter && moment(date).isSame(filter, 'day')) {
        return true;
      } else {
        return false;
      }
    }

    function randomDate(date, filter) {
      var tab = [];
      tab['Tonight'] = moment();
      tab['Tomorrow'] = moment().add(1, 'days');
      if (moment(date).isSame(tab[filter], 'day') && (filter == 'Tonight' || filter == 'Tomorrow')) {
        return true;
      }
      else {
        return false;
      }
    }

    function moneyFilter(money, filter) {
      if (money == 0 && filter == 'Free') {
        return true;
      } else if (money > 0 && money < 100 && filter == '€') {
        return true;
      } else if (money >= 100 && filter == '€€') {
        return true;
      } else {
        return false;
      }
    }

    function weekendFilter(date, filter) {
      var friday = moment().day(5);
      var sunday = moment().day(7);

      if (moment(date).isBetween(friday, sunday) && filter == 'Weekend') {
        return true;
      }
      else {
        return false;
      }
    }

    function typeFilter(type, filters) {

        for (var i = 0; i < type.length; i++) {
          for (var y = 0; y < filters.length; y++) {
            if (type[i] == filters[y]) {
              return true;
            }
          }
        }
    }

    function monthsFilter(dateStart, filters) {
      var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

      if (months[moment(dateStart).month()] == filters) {
        return true;
      } else {
        return false;
      }
    }


    function dayFilter(dateStart, filters) {
      var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

      if (days[moment(dateStart).day()] == filters) {
        return true;
      } else {
        return false;
      }
    }

    return function (events, filters) {
      var filtered = [];
      var sort = [];
      var location = JSON.parse(window.localStorage.getItem('location'));
      var preferences = JSON.parse(window.localStorage.getItem('preferences'));

      /*var longitude = preferences != null && preferences.position.longitude != null ? preferences.position.longitude : location.longitude;
      var latitude = preferences != null && preferences.position.latitude != null ? preferences.position.latitude : location.latitude;
      var mydistance = preferences != null && preferences.distance != null ? preferences.distance : 200;*/

      if (events) {
        for (var i = 0; i < events.length; i++) {
          var event = events[i];
          if (check_array(event, filters) || typeFilter(event.type, filters)  /* &&
            distance(event.place[0].longitude, event.place[0].latitude, longitude, latitude) < mydistance */) {
            filtered.push(event);
          }
        }
      }

      var sorted = function (element) {
        element.sort(function(a, b){
          return moment(a.dateStart) - moment(b.dateStart);
        });
        return element;
      };

      return sorted(filtered);
    }

  })

  .filter('timeSort', function () {
     return function (events){
    events.sort(function(a, b){
      return moment(a.dateStart) - moment(b.dateStart);
    });
    return events;
    }
  })

  .filter('MomentDate', function () {
    return function (date) {
      return moment(date).format("D MMM, HH:mm");
    }
  })
  .filter('Moment', function () {
    return function (date, format) {
      return moment.unix(date).format(format);
    }
  })
  .filter('Linky', function () {
    return function (x) {
      var tree = angular.element('<div>'+ x +'</div>');
      tree.find('a').attr('target', '_self');
      return angular.element('<div>').append(tree).html();
    }
  })
  .filter('UnreadNotif', function () {
    return function (notifs) {
      var unread = 0;
      if (notifs){
      for (var i = 0; i < notifs.length; i++){
        if (notifs[i].read == 'false'){
          unread++;
        }
      }
      }
      return unread;
    }

  })
  .filter('research', function () {
  return function (users, search) {
    if (angular.isDefined(search) && search != ''){
      var filtered = [];
      for (var i = 0;i < users.length;i++){
        if (checkMusic(users[i].organiser.genreMusic, search) || checkMusic(users[i].organiser.typeParty, search) || checkName(users[i], search)) {
          filtered.push(users[i]);
        }
      }
      return filtered;
    }
    return users;
  };

  function checkMusic(tags, search) {
    for (var i = 0; i < tags.length;i++){
      if (tags[i].toLowerCase().indexOf(search.toLowerCase()) != -1){
        return true;
      }
    }
    return false;
  }


  function checkName(user, search) {
    var name = user.local.firstName ? user.local.firstName : user.facebook.firstName;
    if (name.toLowerCase().indexOf(search.toLowerCase()) != -1){
      return true;
    }
    return false;
  }
});
